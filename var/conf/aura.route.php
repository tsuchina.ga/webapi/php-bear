<?php
/* @var $map \Aura\Router\Map */

$map->route('/v1/users', '/v1/users');
$map->route('/v1/users/id', '/v1/users/{id}')->tokens(['id' => '\d+']);
