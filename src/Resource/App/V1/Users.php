<?php
namespace MyVendor\MyProject\Resource\App\V1;

use BEAR\Resource\ResourceObject;
use Ray\AuraSqlModule\AuraSqlInject;

class Users extends ResourceObject
{
    use AuraSqlInject;

    public function onGet() : ResourceObject
    {
        $sql = " SELECT * FROM users WHERE is_deleted = 0 ";
        $sth = $this->pdo->prepare($sql);
        $res = $sth->execute();
        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        $this->body = [
            'users' => $data,
            'method' => 'GET'
        ];
        return $this;
    }

    public function onPost() : ResourceObject
    {
        $this->body = [
            'user' => [],
            'method' => 'POST'
        ];
        return $this;
    }
}
