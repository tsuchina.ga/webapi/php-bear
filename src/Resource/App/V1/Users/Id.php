<?php
namespace MyVendor\MyProject\Resource\App\V1\Users;

use BEAR\Resource\ResourceObject;
use Ray\AuraSqlModule\AuraSqlInject;

class Id extends ResourceObject
{
    use AuraSqlInject;

    public function onGet(int $id) : ResourceObject
    {
        $sql = " SELECT * FROM users WHERE id = :id ";
        $sth = $this->pdo->prepare($sql);
        $sth->bindValue(':id', $id, \PDO::PARAM_INT);
        $res = $sth->execute();
        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        $this->body = [
            'user' => (isset($data[0]) ? $data[0] : []),
            'method' => 'GET'
        ];
        return $this;
    }

    public function onPut(int $id) : ResourceObject
    {
        $this->body = [
            'method' => 'PUT'
        ];
        return $this;
    }

    public function onDelete(int $id) : ResourceObject
    {
        $sql = " UPDATE users SET is_deleted = 1, updated_at = CURRENT_TIMESTAMP WHERE id = :id ";
        $sth = $this->pdo->prepare($sql);
        $sth->bindValue(':id', $id, \PDO::PARAM_INT);
        $res = $sth->execute();
        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        $this->body = [
            'user' => (isset($data[0]) ? $data[0] : []),
            'method' => 'DELETE'
        ];
        return $this;
    }

}
