# php-bear


## 参考にしたサイト

* [BEAR.Sunday](https://bearsunday.github.io/index.html)


## 環境構築

0. dockerコンテナの用意

  ```
  > docker run -d --name php-bear -p 80:80 -p 8080:8080 -v E:/Projects/tsuchinaga/webapi/php-bear:/var/www php:7.2-apache
  > docker exec -it php-bear bash
  ```

0. 初期設定
  0. apache2のrewriteを有効化

    ```
    $ a2enmod rewrite
    ```

  0. psql周りのセットアップ

    ```
    $ apt update -y
    $ apt install -y libpq-dev
    $ docker-php-ext-install pdo pdo_pgsql
    ```

  0. composer

    ```
    $ cp composer.phar /usr/local/bin/composer
    $ apt install -y git
    $ apt install -y zip unzip
    ```

0. クイックスタート

  0. skeletonのセットアップ

    ```
    $ composer create-project -n bear/skeleton tsuchinaga.bear
    ```

  0. DocumentRootの更新

    ```
    $ cd /var/www/tsuchinaga.bear/
    $ cp 000-default.conf /etc/apache2/sites-available/
    ```

  0. Moduleの追加

  ```
  $ cd /var/www/tsuchinaga.bear/
  $ composer require bear/aura-router-module
  ```


  0. ログアウトしてdocker restert

    ```
    > docker restart php-bear
    ```

0. データベースアクセス

  0. Moduleの追加

  ```
  $ composer require ray/aura-sql-module
  ```
